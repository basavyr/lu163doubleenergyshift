#include "../include/energies.h"
#include <iostream>
#include <cmath>
#include <vector>
#include <algorithm>
#include <functional>

//object for Experimental data
ExperimentalEnergies Lu163Exp;
TheoreticalEnergies Lu163Th;

double returnEnergyShifted(double x)
{
    if (x > 10)
        return x + TheoreticalEnergies::bandShift14;
    return x + TheoreticalEnergies::bandShift23;
}

void newLine()
{
    std::cout << "\n";
}

double TheoreticalEnergies::inertiaMoment(double izero, double gamma, int k)
{
    return (double)izero / (1.0 + sqrt(5.0 / (16.0 * PI)) * BETA) * (1.0 - sqrt(5.0 / (4.0 * PI)) * BETA * cos(gamma + 2.0 / 3.0 * PI * k));
}

double TheoreticalEnergies::inertiaFactor1(double izero, double gamma)
{
    return (double)1.0 / (2.0 * inertiaMoment(izero, gamma, 1));
}

double TheoreticalEnergies::inertiaFactor2(double izero, double gamma)
{
    return (double)1.0 / (2.0 * inertiaMoment(izero, gamma, 2));
}

double TheoreticalEnergies::inertiaFactor3(double izero, double gamma)
{
    return (double)1.0 / (2.0 * inertiaMoment(izero, gamma, 3));
}

double TheoreticalEnergies::subTerm1(double spin, double oddSpin, double izero, double gamma)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    return (2.0 * spin - 1.0) * (a3 - a1) + 2.0 * oddSpin * a1;
}

double TheoreticalEnergies::subTerm2(double spin, double oddSpin, double izero, double gamma)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    return (2.0 * spin - 1.0) * (a2 - a1) + 2.0 * oddSpin * a1;
}

double TheoreticalEnergies::subTerm3(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    double sing, cosg, rad3;
    rad3 = sqrt(3.0);
    sing = sin(gamma);
    cosg = cos(gamma);
    return (2.0 * oddSpin - 1.0) * (a3 - a1) + 2.0 * spin * a1 + particlePotential * (2.0 * oddSpin - 1.0) / (oddSpin * (oddSpin + 1.0)) * rad3 * (rad3 * cosg + sing);
}

double TheoreticalEnergies::subTerm4(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    double sing, cosg, rad3;
    rad3 = sqrt(3.0);
    sing = sin(gamma);
    cosg = cos(gamma);
    return (2.0 * oddSpin - 1.0) * (a2 - a1) + 2.0 * spin * a1 + particlePotential * (2.0 * oddSpin - 1.0) / (oddSpin * (oddSpin + 1.0)) * 2.0 * rad3 * sing;
}

double TheoreticalEnergies::bigTerm1(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    return -1.0 * (subTerm1(spin, oddSpin, izero, gamma) * subTerm2(spin, oddSpin, izero, gamma) + 8.0 * a2 * a3 * spin * oddSpin + subTerm3(spin, oddSpin, izero, gamma, particlePotential) * subTerm4(spin, oddSpin, izero, gamma, particlePotential));
}

double TheoreticalEnergies::bigTerm2(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    return (subTerm1(spin, oddSpin, izero, gamma) * subTerm3(spin, oddSpin, izero, gamma, particlePotential) - 4.0 * spin * oddSpin * a3 * a3) * (subTerm2(spin, oddSpin, izero, gamma) * subTerm4(spin, oddSpin, izero, gamma, particlePotential) - 4.0 * spin * oddSpin * a2 * a2);
}

double TheoreticalEnergies::Omega1(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    return sqrt(0.5 * (-bigTerm1(spin, oddSpin, izero, gamma, particlePotential) + pow(pow(bigTerm1(spin, oddSpin, izero, gamma, particlePotential), 2) - 4.0 * bigTerm2(spin, oddSpin, izero, gamma, particlePotential), 0.5)));
}

double TheoreticalEnergies::Omega2(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    return sqrt(0.5 * (-bigTerm1(spin, oddSpin, izero, gamma, particlePotential) - pow(pow(bigTerm1(spin, oddSpin, izero, gamma, particlePotential), 2) - 4.0 * bigTerm2(spin, oddSpin, izero, gamma, particlePotential), 0.5)));
}

double TheoreticalEnergies::minHamiltonian(double spin, double oddSpin, double izero, double gamma, double particlePotential)
{
    double sing;
    sing = sin(gamma + PI / 6.0);
    a1 = inertiaFactor1(izero, gamma);
    a2 = inertiaFactor2(izero, gamma);
    a3 = inertiaFactor3(izero, gamma);
    return (a2 + a3) * (spin + oddSpin) / 2.0 + a1 * pow(spin - oddSpin, 2) - particlePotential * (2.0 * oddSpin - 1.0) / (oddSpin + 1.0) * sing;
}

double TheoreticalEnergies::TSD1_Th(double spin, double izero, double particlePotential)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;
    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spin, oddSpin, izero, gamma, particlePotential) + Omega2(spin, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + bandShift14;
}

double TheoreticalEnergies::TSD2_Th(double spin, double izero, double particlePotential)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 1.0, oddSpin, izero, gamma, particlePotential) + 0.5 * (3.0 * Omega1(spin - 1.0, oddSpin, izero, gamma, particlePotential) + Omega2(spin - 1.0, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + bandShift23;
}

double TheoreticalEnergies::TSD3_Th(double spin, double izero, double particlePotential)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 2.0, oddSpin, izero, gamma, particlePotential) + 0.5 * (5.0 * Omega1(spin - 2.0, oddSpin, izero, gamma, particlePotential) + Omega2(spin - 2.0, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + bandShift23;
}

double TheoreticalEnergies::TSD4_Th(double spin, double izero, double particlePotential)
{
    double rez, eZero, spinZero, oddSpin, secondSpin;
    double singleParticleEnergy = -0.334;
    spinZero = 6.5;
    oddSpin = 6.5;
    secondSpin = 4.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 3.0, secondSpin, izero, gamma, particlePotential) + 0.5 * (7.0 * Omega1(spin - 3.0, secondSpin, izero, gamma, particlePotential) + Omega2(spin - 3.0, secondSpin, izero, gamma, particlePotential));

    return rez - eZero + singleParticleEnergy - bandShift14 + bandShift23;
}

double TheoreticalEnergies::TSD1_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;
    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spin, oddSpin, izero, gamma, particlePotential) + Omega2(spin, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + shift2;
}

double TheoreticalEnergies::TSD2_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 1.0, oddSpin, izero, gamma, particlePotential) + 0.5 * (3.0 * Omega1(spin - 1.0, oddSpin, izero, gamma, particlePotential) + Omega2(spin - 1.0, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + shift1;
}

double TheoreticalEnergies::TSD3_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2)
{
    double rez, eZero, spinZero, oddSpin;
    spinZero = 6.5;
    oddSpin = 6.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 2.0, oddSpin, izero, gamma, particlePotential) + 0.5 * (5.0 * Omega1(spin - 2.0, oddSpin, izero, gamma, particlePotential) + Omega2(spin - 2.0, oddSpin, izero, gamma, particlePotential));

    return rez - eZero + shift1;
}

double TheoreticalEnergies::TSD4_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2)
{
    double rez, eZero, spinZero, oddSpin, secondSpin;
    double singleParticleEnergy = -0.334;
    spinZero = 6.5;
    oddSpin = 6.5;
    secondSpin = 4.5;
    const double gamma = 17.0 * PI / 180.0;

    //excitation energy of the first TSD1 band I=13/2
    eZero = minHamiltonian(spinZero, oddSpin, izero, gamma, particlePotential) + 0.5 * (Omega1(spinZero, oddSpin, izero, gamma, particlePotential) + Omega2(spinZero, oddSpin, izero, gamma, particlePotential));

    //ACTUAL ENERGY OF THE BAND LEVEL I
    rez = minHamiltonian(spin - 3.0, secondSpin, izero, gamma, particlePotential) + 0.5 * (7.0 * Omega1(spin - 3.0, secondSpin, izero, gamma, particlePotential) + Omega2(spin - 3.0, secondSpin, izero, gamma, particlePotential));

    return rez - eZero + singleParticleEnergy - shift2 + shift1;
}

void RMS_Calculus::showValues(double exp[], double th[], int size)
{
    std::vector<double> x1(exp, exp + size);
    std::vector<double> x2(th, th + size);
    auto printer = [](std::vector<double> x1) {
        for (auto &i : x1)
        {
            std::cout << i;
            newLine();
        }
    };
    auto adder = [](std::vector<double> x1, std::vector<double> x2) {
        std::transform(x1.begin(), x1.end(), x2.begin(), x1.begin(), std::plus<double>());
    };
    printer(x1);
    /* 
    for (auto &i : x1)
    {
        std::cout << i << "\n";
    }
    newLine(); */
}

//Squared difference of the energies
double RMS_Calculus::SquaredSum(std::vector<double> exp, std::vector<double> th)
{
    if (exp.size() != th.size())
        return 0;

    double result = 0;
    auto sumSquared = [](auto x, auto y) {
        return pow(x - y, 2.0);
    };
    for (int i = 0; i < exp.size(); ++i)
        result += sumSquared(exp.at(i), th.at(i));
    return result;
}

double RMS_Calculus::rmsValue(std::vector<double> exp, std::vector<double> th)
{
    if (exp.size() != th.size())
        return 0;
    return sqrt(SquaredSum(exp, th) / exp.size());
}

//Compute the RMS value for a set of Experimental and Theoretical Energies.
//Function depends only on the parameters I0 and V
double RMS_Calculus::rootMeanSquare(double izero, double particlePotential)
{
    double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0;

    //squared difference of energy values
    auto sumSquared = [](auto x, auto y) {
        return pow(x - y, 2.0);
    };

    bool ok = true;
    int count = 0;
    while (ok)
    {
        for (int i = 0; i < Lu163Exp.dim1 && ok; ++i)
        {
            double temp = Lu163Th.TSD1_Th(Lu163Exp.Spin1[i], izero, particlePotential);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum1 += sumSquared(Lu163Exp.TSD1_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim2 && ok; ++i)
        {
            double temp = Lu163Th.TSD2_Th(Lu163Exp.Spin2[i], izero, particlePotential);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum2 += sumSquared(Lu163Exp.TSD2_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim3 && ok; ++i)
        {
            double temp = Lu163Th.TSD3_Th(Lu163Exp.Spin3[i], izero, particlePotential);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum3 += sumSquared(Lu163Exp.TSD3_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim4 && ok; ++i)
        {
            double temp = Lu163Th.TSD4_Th(Lu163Exp.Spin4[i], izero, particlePotential);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum4 += sumSquared(Lu163Exp.TSD4_Experimental[i], temp);
                count++;
            }
        }
        if (ok)
            ok = !ok;
    }
    if (sum1 && sum2 && sum3 && sum4 && count == (Lu163Exp.dim1 + Lu163Exp.dim2 + Lu163Exp.dim3 + Lu163Exp.dim4))
        return sqrt(1.0 / count * (sum1 + sum2 + sum3 + sum4));
    return 9876543210.0;
}

double RMS_Calculus::rootMeanSquareWithShift(double izero, double particlePotential, double shift1, double shift2)
{
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    double sum1 = 0.0, sum2 = 0.0, sum3 = 0.0, sum4 = 0.0;

    //squared difference of energy values
    auto sumSquared = [](auto x, auto y) {
        return pow(x - y, 2.0);
    };

    bool ok = true;
    int count = 0;
    while (ok)
    {
        for (int i = 0; i < Lu163Exp.dim1 && ok; ++i)
        {
            double temp = Lu163Th.TSD1_Th_Shifted(Lu163Exp.Spin1[i], izero, particlePotential, shift1, shift2);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum1 += sumSquared(Lu163Exp.TSD1_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim2 && ok; ++i)
        {
            double temp = Lu163Th.TSD2_Th_Shifted(Lu163Exp.Spin2[i], izero, particlePotential, shift1, shift2);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum2 += sumSquared(Lu163Exp.TSD2_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim3 && ok; ++i)
        {
            double temp = Lu163Th.TSD3_Th_Shifted(Lu163Exp.Spin3[i], izero, particlePotential, shift1, shift2);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum3 += sumSquared(Lu163Exp.TSD3_Experimental[i], temp);
                count++;
            }
        }
        for (int i = 0; i < Lu163Exp.dim4 && ok; ++i)
        {
            double temp = Lu163Th.TSD4_Th_Shifted(Lu163Exp.Spin4[i], izero, particlePotential, shift1, shift2);
            if (isnan(temp))
            {
                ok = 0;
                break;
            }
            else
            {
                sum4 += sumSquared(Lu163Exp.TSD4_Experimental[i], temp);
                count++;
            }
        }
        if (ok)
            ok = !ok;
    }
    if (sum1 && sum2 && sum3 && sum4 && count == (Lu163Exp.dim1 + Lu163Exp.dim2 + Lu163Exp.dim3 + Lu163Exp.dim4))
        return sqrt(1.0 / count * (sum1 + sum2 + sum3 + sum4));
    return 9876543210.0;
}