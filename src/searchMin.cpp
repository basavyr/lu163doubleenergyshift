#include "../include/searchMin.h"
#include "../include/energies.h"
#include <cmath>
#include <iostream>
#include <vector>
#include <string>
#include <chrono>

//writing to file
#include <fstream>

std::ofstream output("/home/robert.poenaru/PIPELINE/DevWorkspace/C++/lu163doubleenergyshift/output/data.out");
std::ofstream plotData("/home/robert.poenaru/PIPELINE/DevWorkspace/C++/lu163doubleenergyshift/output/mathematicaPlots.out");

//objects for non-static functions
RMS_Calculus rms1;
ExperimentalEnergies lu163exp;
TheoreticalEnergies lu163th;
GenerateResults results;
SearchMinValue minvalue;

//Calculates the minimum value for the RMS function
//E_exp and E_th data sets
//Two fit parameters: I0 and V

//numerical limits for the fit parameters
typedef struct SearchMinValue::parameterLimits
{
    const double izero_left = 20.0;
    const double izero_right = 100.0;
    const double izero_step = 1.0;
    const double particlePotential_left = 0.01;
    const double particlePotential_right = 20.0;
    const double particlePotential_step = 0.01;
} plim;

//parameter limit declaration
plim lims;

void SearchMinValue::findMinimum()
{
    // double izero;
    // double particlePotential;
    double minValue = 987654321.0;
    double izeroMIN = 0;
    double particlePotentialMIN = 0;
    for (double izero = lims.izero_left; izero <= lims.izero_right; izero += lims.izero_step)
    {
        for (double particlePotential = lims.particlePotential_left; particlePotential <= lims.particlePotential_right; particlePotential += lims.particlePotential_step)
        {
            double temp = rms1.rootMeanSquare(izero, particlePotential);
            if (!isnan(temp) && temp <= minValue)
            {
                minValue = temp;
                izeroMIN = izero;
                particlePotentialMIN = particlePotential;
            }
        }
    }
    results.showResults(izeroMIN, particlePotentialMIN, minValue);
    // showEnergies(izeroMIN, particlePotentialMIN);
}

void SearchMinValue::findMinimumWithShift(double shift1, double shift2)
{
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    double minValue = 987654321.0;
    double izeroMIN = 0;
    double particlePotentialMIN = 0;

    //time measurement benchmark
    auto startTime = std::chrono::high_resolution_clock::now();
    for (double izero = lims.izero_left; izero <= lims.izero_right; izero += lims.izero_step)
    {
        for (double particlePotential = lims.particlePotential_left; particlePotential <= lims.particlePotential_right; particlePotential += lims.particlePotential_step)
        {
            double temp = rms1.rootMeanSquareWithShift(izero, particlePotential, shift1, shift2);
            if (!isnan(temp) && temp <= minValue)
            {
                minValue = temp;
                izeroMIN = izero;
                particlePotentialMIN = particlePotential;
            }
        }
    }
    auto endTime = std::chrono::high_resolution_clock::now();

    results.showResults(izeroMIN, particlePotentialMIN, minValue);
    results.showResultsToFile(izeroMIN, particlePotentialMIN, minValue, shift2);

    //process duration
    auto exectime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
    std::cout << "Finding minimum took " << (double)exectime / 1000.0 << " seconds";
    newLine();
    // results.showEnergies_CustomShift(izeroMIN, particlePotentialMIN, shift1, shift2);
}

void SearchMinValue::findMinimumWithShift_Multiplex(double shift1, double shift2, double *bestFit, double *bestIZero, double *bestParticlePotential)
{
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    double minValue = 987654321.0;
    double izeroMIN = 0;
    double particlePotentialMIN = 0;

    //time measurement benchmark
    auto startTime = std::chrono::high_resolution_clock::now();
    for (double izero = lims.izero_left; izero <= lims.izero_right; izero += lims.izero_step)
    {
        for (double particlePotential = lims.particlePotential_left; particlePotential <= lims.particlePotential_right; particlePotential += lims.particlePotential_step)
        {
            double temp = rms1.rootMeanSquareWithShift(izero, particlePotential, shift1, shift2);
            if (!isnan(temp) && temp <= minValue)
            {
                minValue = temp;
                izeroMIN = izero;
                particlePotentialMIN = particlePotential;
            }
        }
    }
    auto endTime = std::chrono::high_resolution_clock::now();

    //updates the variabiles in the reapeated shift-dependent Minimum calculus
    *bestFit = minValue;
    *bestIZero = izeroMIN;
    *bestParticlePotential = particlePotentialMIN;

    results.showResults(izeroMIN, particlePotentialMIN, minValue);
    results.showResultsToFile_Multiplex(izeroMIN, particlePotentialMIN, minValue, shift1, shift2);

    //process duration
    auto exectime = std::chrono::duration_cast<std::chrono::milliseconds>(endTime - startTime).count();
    std::cout << "Finding minimum took " << (double)exectime / 1000.0 << " seconds";
    newLine();
    output << "Finding minimum took " << (double)exectime / 1000.0 << " seconds";
    results.newLineToFile();
    // results.showEnergies_CustomShift(izeroMIN, particlePotentialMIN, shift1, shift2);
}

void GenerateResults::newLineToFile()
{
    output << "\n";
}

void GenerateResults::showResults(double x, double y, double z)
{
    std::cout << "*******************************";
    newLine();
    std::cout << "The results for Lu163 are:";
    newLine();
    std::cout << "I_0 = " << x;
    newLine();
    std::cout << "V = " << y;
    newLine();
    std::cout << "RMS = " << z;
    newLine();
    std::cout << "*******************************";
    newLine();
}

void GenerateResults::showResultsToFile(double x, double y, double z, double shift)
{
    output << "Shift = " << shift;
    newLineToFile();
    output << "*******************************";
    newLineToFile();
    output << "The results for Lu163 are:";
    newLineToFile();
    output << "I_0 = " << x;
    newLineToFile();
    output << "V = " << y;
    newLineToFile();
    output << "RMS = " << z;
    newLineToFile();
    output << "*******************************";
    newLineToFile();
}

void GenerateResults::showResultsToFile_Multiplex(double x, double y, double z, double shift1, double shift2)
{
    output << "Shift1 = " << shift1;
    newLineToFile();
    output << "Shift2 = " << shift2;
    newLineToFile();
    output << "*******************************";
    newLineToFile();
    output << "The results for Lu163 are:";
    newLineToFile();
    output << "I_0 = " << x;
    newLineToFile();
    output << "V = " << y;
    newLineToFile();
    output << "RMS = " << z;
    newLineToFile();
    output << "*******************************";
    newLineToFile();
}

void GenerateResults::showFinalResultsToFile_Multiplex(double a1, double a2, double a3, double a4, double a5)
{
    GenerateResults res;
    output << "************************ FINAL RESULTS ************************";
    res.newLineToFile();
    output << "I0  V   RMS   es1  es2";
    res.newLineToFile();
    output << a1 << " " << a2 << " " << a3 << " " << a4 << " " << a5;
    res.newLineToFile();
}

//Unifies the Experimental data
void SearchMinValue::createEnergySets(std::vector<double> *spins, std::vector<double> *energies)
{
    for (int i = 0; i < lu163exp.dim1; ++i)
    {
        spins->push_back(lu163exp.Spin1[i]);
        energies->push_back(lu163exp.TSD1_Experimental[i]);
    }
    for (int i = 0; i < lu163exp.dim2; ++i)
    {
        spins->push_back(lu163exp.Spin2[i]);
        energies->push_back(lu163exp.TSD2_Experimental[i]);
    }
    for (int i = 0; i < lu163exp.dim3; ++i)
    {
        spins->push_back(lu163exp.Spin3[i]);
        energies->push_back(lu163exp.TSD3_Experimental[i]);
    }
    for (int i = 0; i < lu163exp.dim4; ++i)
    {
        spins->push_back(lu163exp.Spin4[i]);
        energies->push_back(lu163exp.TSD4_Experimental[i]);
    }
}

void SearchMinValue::createTheoreticalDataSet(double izero, double particlePotential, std::vector<double> *energies)
{
    for (int i = 0; i < lu163exp.dim1; i++)
    {
        energies->push_back(lu163th.TSD1_Th(lu163exp.Spin1[i], izero, particlePotential));
    }
    for (int i = 0; i < lu163exp.dim2; i++)
    {
        energies->push_back(lu163th.TSD2_Th(lu163exp.Spin2[i], izero, particlePotential));
    }
    for (int i = 0; i < lu163exp.dim3; i++)
    {
        energies->push_back(lu163th.TSD3_Th(lu163exp.Spin3[i], izero, particlePotential));
    }
    for (int i = 0; i < lu163exp.dim4; i++)
    {
        energies->push_back(lu163th.TSD4_Th(lu163exp.Spin4[i], izero, particlePotential));
    }
}

void SearchMinValue::createTheoreticalDataSet_CustomShift(double izero, double particlePotential, std::vector<double> *energies, double shift1, double shift2)
{
    for (int i = 0; i < lu163exp.dim1; i++)
    {
        energies->push_back(lu163th.TSD1_Th_Shifted(lu163exp.Spin1[i], izero, particlePotential, shift1, shift2));
    }
    for (int i = 0; i < lu163exp.dim2; i++)
    {
        energies->push_back(lu163th.TSD2_Th_Shifted(lu163exp.Spin2[i], izero, particlePotential, shift1, shift2));
    }
    for (int i = 0; i < lu163exp.dim3; i++)
    {
        energies->push_back(lu163th.TSD3_Th_Shifted(lu163exp.Spin3[i], izero, particlePotential, shift1, shift2));
    }
    for (int i = 0; i < lu163exp.dim4; i++)
    {
        energies->push_back(lu163th.TSD4_Th_Shifted(lu163exp.Spin4[i], izero, particlePotential, shift1, shift2));
    }
}

void GenerateResults::writeNumbers(double x1, double x2, double x3)
{
    std::cout << x1 << " " << x2 << " " << x3;
    newLine();
    output << x1 << " " << x2 << " " << x3;
    newLineToFile();
}

//Ouputs the two sets of energies for a given set of parameters I0 and V with predefined energy shifts
void GenerateResults::showEnergies(double izero, double particlePotential)
{
    //arrays for spin data
    std::vector<double> spins;
    //arrays for exp data
    std::vector<double> energiesExp;
    //arrays for th data
    std::vector<double> energiesTh;

    minvalue.createEnergySets(&spins, &energiesExp);
    minvalue.createTheoreticalDataSet(izero, particlePotential, &energiesTh);

    for (int i = 0; i < spins.size(); ++i)
    {
        writeNumbers(spins.at(i), energiesExp.at(i), energiesTh.at(i));
        /* 
        std::cout << spins.at(i) << " " << energiesExp.at(i) << " " << energiesTh.at(i);
        newLine();
        */
    }
    std::cout << rms1.rmsValue(energiesExp, energiesTh);
    newLine();
}

//Ouputs the two sets of energies for a given set of parameters I0 and V with selected energy shifts
void GenerateResults::showEnergies_CustomShift(double izero, double particlePotential, double shift1, double shift2)
{
    //arrays for spin data
    std::vector<double> spins;
    //arrays for exp data
    std::vector<double> energiesExp;
    //arrays for th data shifted manually
    std::vector<double> energiesTh_CustomShift;

    minvalue.createEnergySets(&spins, &energiesExp);
    minvalue.createTheoreticalDataSet_CustomShift(izero, particlePotential, &energiesTh_CustomShift, shift1, shift2);

    for (int i = 0; i < spins.size(); ++i)
    {
        writeNumbers(spins.at(i), energiesExp.at(i), energiesTh_CustomShift.at(i));
        /* 
        std::cout << spins.at(i) << " " << energiesExp.at(i) << " " << energiesTh.at(i);
        newLine();
        */
    }
    std::cout << rms1.rmsValue(energiesExp, energiesTh_CustomShift);
    newLine();
    generateMathematicaOutput(spins, energiesExp, energiesTh_CustomShift);
}

void GenerateResults::generateMathematicaOutput(std::vector<double> spins, std::vector<double> exp, std::vector<double> th)
{
    std::vector<double> spin1;
    std::vector<double> spin2;
    std::vector<double> spin3;
    std::vector<double> spin4;
    std::vector<double> tsd1Exp;
    std::vector<double> tsd2Exp;
    std::vector<double> tsd3Exp;
    std::vector<double> tsd4Exp;
    std::vector<double> tsd1Th;
    std::vector<double> tsd2Th;
    std::vector<double> tsd3Th;
    std::vector<double> tsd4th;

    const std::string sp1 = "spin1 = ";
    const std::string sp2 = "spin2 = ";
    const std::string sp3 = "spin3 = ";
    const std::string sp4 = "spin4 = ";
    const std::string e1exp = "tsd1Exp = ";
    const std::string e2exp = "tsd2Exp = ";
    const std::string e3exp = "tsd3Exp = ";
    const std::string e4exp = "tsd4Exp = ";
    const std::string e1th = "tsd1Th = ";
    const std::string e2th = "tsd2Th = ";
    const std::string e3th = "tsd3Th = ";
    const std::string e4th = "tsd4Th = ";

    //EXPERIMENTAL DATA
    //SPINS
    generateBands(spins, &spin1, 0, lu163exp.dim1, sp1);
    generateBands(spins, &spin2, lu163exp.dim1, lu163exp.dim1 + lu163exp.dim2, sp2);
    generateBands(spins, &spin3, lu163exp.dim1 + lu163exp.dim2, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, sp3);
    generateBands(spins, &spin4, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3 + lu163exp.dim4, sp4);

    //TSD1 exp
    generateBands(exp, &tsd1Exp, 0, lu163exp.dim1, e1exp);
    //TSD2 exp
    generateBands(exp, &tsd2Exp, lu163exp.dim1, lu163exp.dim1 + lu163exp.dim2, e2exp);
    //TSD3 exp
    generateBands(exp, &tsd3Exp, lu163exp.dim1 + lu163exp.dim2, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, e3exp);
    //TSD4 exp
    generateBands(exp, &tsd4Exp, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3 + lu163exp.dim4, e4exp);

    //THEORETICAL DATA
    //TSD1 exp
    generateBands(th, &tsd1Th, 0, lu163exp.dim1, e1th);
    //TSD2 exp
    generateBands(th, &tsd2Th, lu163exp.dim1, lu163exp.dim1 + lu163exp.dim2, e2th);
    //TSD3 exp
    generateBands(th, &tsd3Th, lu163exp.dim1 + lu163exp.dim2, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, e3th);
    //TSD4 exp
    generateBands(th, &tsd4th, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3, lu163exp.dim1 + lu163exp.dim2 + lu163exp.dim3 + lu163exp.dim4, e4th);

    /*     plotData << "{";
    for (auto &i : spins)
    {
        plotData << i << " , ";
    }
    plotData << "};";
    plotData << "\n";

    plotData << "{";
    for (auto &i : exp)
    {
        plotData << i << " , ";
    }
    plotData << "};";
    plotData << "\n";

    plotData << "{";
    for (auto &i : th)
    {
        plotData << i << " , ";
    }
    plotData << "};";
    plotData << "\n";
 */
}

void GenerateResults::generateBands(std::vector<double> initArray, std::vector<double> *finalArray, int left, int right, const std::string &text)
{
    for (int i = left; i < right; ++i)
    {
        finalArray->push_back(initArray.at(i));
    }
    plotData << text << "{";
    for (auto i : *finalArray)
    {
        plotData << i;
        if (i != finalArray->at(finalArray->size() - 1))
            plotData << " , ";
    }
    plotData << "};";
    plotData << "\n";
}