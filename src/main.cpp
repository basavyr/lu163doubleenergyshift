#include <iostream>
#include "../include/energies.h"
#include "../include/searchMin.h"

#include <omp.h>

using namespace std;

int main()
{
    //struct for keeping the best fitting parameters and the best energy shifts from the ITERATED minimum function
    struct minimalResults
    {
        double rms;
        double izero;
        double particlePotential;
        double shift1;
        double shift2;
    } mins;
    mins.rms = 987654321.0;

    /* 
    for (double shift1 = 0; shift1 <= 1.5; shift1 += 0.25)
    {
        for (double shift2 = 0; shift2 <= 1.5; shift2 += 0.25)
        {
            SearchMinValue minval;
            double bestFit;
            double bestIZero, bestParticlePotential;
            cout << "Shift1 = " << shift1;
            newLine();
            cout << "Shift2 = " << shift2;
            newLine();
            minval.findMinimumWithShift_Multiplex(shift1, shift2, &bestFit, &bestIZero, &bestParticlePotential);
            newLine();
            if (bestFit < mins.rms)
            {
                mins.rms = bestFit;
                mins.izero = bestIZero;
                mins.particlePotential = bestParticlePotential;
                mins.shift1 = shift1;
                mins.shift2 = shift2;
            }
        }
    }

    cout << "************************ FINAL RESULTS ************************";
    newLine();
    cout << mins.izero << " " << mins.particlePotential << " " << mins.rms << " " << mins.shift1 << " " << mins.shift2;
    newLine();
    GenerateResults::showFinalResultsToFile_Multiplex(mins.izero, mins.particlePotential, mins.rms, mins.shift1, mins.shift2); */
    GenerateResults res;
    res.showEnergies_CustomShift(56, 0.18, 0.75, 0.75);
}