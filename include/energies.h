#ifndef ENERGIES_H
#define ENERGIES_H

#include <vector>

double returnEnergyShifted(double);
void newLine();

class TheoreticalEnergies
{
private:
    const double BETA = 0.38;
    double a1, a2, a3;
    double inertiaMoment(double izero, double gamma, int k);
    double inertiaFactor1(double izero, double gamma);
    double inertiaFactor2(double izero, double gamma);
    double inertiaFactor3(double izero, double gamma);
    double subTerm1(double spin, double oddSpin, double izero, double gamma);
    double subTerm2(double spin, double oddSpin, double izero, double gamma);
    double subTerm3(double spin, double oddSpin, double izero, double gamma, double particlePotential);
    double subTerm4(double spin, double oddSpin, double izero, double gamma, double particlePotential);
    double bigTerm1(double spin, double oddSpin, double izero, double gamma, double particlePotential);
    double bigTerm2(double spin, double oddSpin, double izero, double gamma, double particlePotential);

public:
    double minHamiltonian(double spin, double oddSpin, double izero, double gamma, double particlePotential);
    double Omega1(double spin, double oddSpin, double izero, double gamma, double particlePotential);
    double Omega2(double spin, double oddSpin, double izero, double gamma, double particlePotential);

    //ENERGIES WITH DOUBLE SHIFT (0.4 for TSD2,3 and 0.5 for TSD1,4)
public:
    static constexpr double PI = 3.141592654;

    //energy shift for the bands 1 4
    static constexpr double bandShift14 = 0.500;
    //energy shift for the bands 2 3
    static constexpr double bandShift23 = 0.414286;

    //THEORETICAL ENERGIES UNSHIFTED (1 SHIFT) BAND1
    double TSD1_Th(double spin, double izero, double particlePotential);
    //THEORETICAL ENERGIES UNSHIFTED (1 SHIFT) BAND2
    double TSD2_Th(double spin, double izero, double particlePotential);
    //THEORETICAL ENERGIES UNSHIFTED (1 SHIFT) BAND3
    double TSD3_Th(double spin, double izero, double particlePotential);
    //THEORETICAL ENERGIES UNSHIFTED (1 SHIFT) BAND4
    double TSD4_Th(double spin, double izero, double particlePotential);

    //THEORETICAL ENERGIES SHIFTED (2 ENERGY SHIFTS) BAND1
    double TSD1_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2);
    //THEORETICAL ENERGIES SHIFTED (2 ENERGY SHIFTS) BAND2
    double TSD2_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2);
    //THEORETICAL ENERGIES SHIFTED (2 ENERGY SHIFTS) BAND3
    double TSD3_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2);
    //THEORETICAL ENERGIES SHIFTED (2 ENERGY SHIFTS) BAND4
    double TSD4_Th_Shifted(double spin, double izero, double particlePotential, double shift1, double shift2);
};

class ExperimentalEnergies
{
private:
    /* data */
public:
    //energies
    double TSD1_Experimental[21] = {0.1966, 0.4597, 0.7746, 1.1609, 1.6112, 2.1265, 2.7051, 3.3441, 4.0411, 4.7937, 5.5992, 6.457, 7.3667, 8.3293, 9.3458, 10.4169, 11.5431, 12.7224, 13.9491, 15.2181, 16.5221};
    double TSD2_Experimental[17] = {1.3394, 1.7467, 2.2184, 2.7527, 3.3484, 4.003, 4.7143, 5.4805, 6.3004, 7.1733, 8.0998, 9.08, 10.1147, 11.2036, 12.3466, 13.5441, 14.7911};
    double TSD3_Experimental[14] = {2.1237, 2.6293, 3.1973, 3.8243, 4.5094, 5.2506, 6.0465, 6.8963, 7.7988, 8.7546, 9.7638, 10.8268, 11.9392, 13.0861};
    double TSD4_Experimental[10] = {4.58, 5.2251, 5.9273, 6.6819, 7.4919, 8.3573, 9.2778, 10.2535, 11.2851, 12.3701};

    //spins
    double Spin1[21] = {8.5, 10.5, 12.5, 14.5, 16.5, 18.5, 20.5, 22.5, 24.5, 26.5, 28.5, 30.5, 32.5, 34.5, 36.5, 38.5, 40.5, 42.5, 44.5, 46.5, 48.5};
    double Spin2[17] = {13.5, 15.5, 17.5, 19.5, 21.5, 23.5, 25.5, 27.5, 29.5, 31.5, 33.5, 35.5, 37.5, 39.5, 41.5, 43.5, 45.5};
    double Spin3[14] = {16.5, 18.5, 20.5, 22.5, 24.5, 26.5, 28.5, 30.5, 32.5, 34.5, 36.5, 38.5, 40.5, 42.5};
    double Spin4[10] = {23.5, 25.5, 27.5, 29.5, 31.5, 33.5, 35.5, 37.5, 39.5, 41.5};
    static constexpr int dim1 = 21;
    static constexpr int dim2 = 17;
    static constexpr int dim3 = 14;
    static constexpr int dim4 = 10;
};

class RMS_Calculus
{
public:
    //Squared difference of the energies
    static double SquaredSum(std::vector<double> exp, std::vector<double> th);
    //rms of the energies (from given sets of data)
    static double rmsValue(std::vector<double> exp, std::vector<double> th);

    static void showValues(double[], double[], int);
    //rms of the energies (from paramteres)
    static double rootMeanSquare(double, double);

    //rms of the energies (from paramteres) with SHIFT1 and SHIFT2 for the bands 1,2,3,4
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    static double rootMeanSquareWithShift(double, double, double, double);
};

#endif // ENERGIES_H
