#ifndef SEARCHMIN_H
#define SEARCHMIN_H

#include <vector>
#include <string>
class SearchMinValue
{
private:
    /* data */
public:
    //numerical limits for the fit parameters
    struct parameterLimits;

    //Computes the minimum value of RMS function with respect to the entire set of parameters I_0 and V.
    //The energy functions have predefined energy shifts.
    void findMinimum();

    //Computes the minimum value of RMS function with respect to the entire set of parameters I_0 and V.
    //The energy functions have two shifts manually chosen.
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    void findMinimumWithShift(double, double);

    //ITERATED PROCEDURE
    //Computes the minimum value of RMS function with respect to the entire set of parameters I_0 and V.
    //The energy functions have two shifts manually chosen.
    //The function updates best fit values after each runtime
    //shift1: BANDS 2,3,4
    //shift2: BANDS 1,4
    void findMinimumWithShift_Multiplex(double, double, double *, double *, double *);

    //Unifies the Experimental data
    void createEnergySets(std::vector<double> *, std::vector<double> *);
    //creates Unified Theoretical Data set with predefined energy shifts
    void createTheoreticalDataSet(double, double, std::vector<double> *);

    //creates Unified Theoretical Data set with selected energy shifts
    void createTheoreticalDataSet_CustomShift(double, double, std::vector<double> *, double, double);
};
//Class for showing results in formatted output
class GenerateResults
{
private:
    /* data */
public:
    //new line to file
    void newLineToFile();

    void showResults(double, double, double);
    void showResultsToFile(double, double, double, double);
    void showResultsToFile_Multiplex(double, double, double, double, double);
    static void showFinalResultsToFile_Multiplex(double, double, double, double, double);
    void showEnergies(double, double);
    void showEnergies_CustomShift(double, double, double, double);

    void generateMathematicaOutput(std::vector<double>, std::vector<double>, std::vector<double>);
    void generateBands(std::vector<double>, std::vector<double> *, int, int, const std::string &);

    void writeNumbers(double, double, double);
};

#endif // SEARCHMIN_H
